# README #

This README would normally document whatever steps are necessary to get your application up and running.

## メイン画面 ##

- 納品日以外の項目は必須項目とする
- 納品日が未入力の場合、ステータスに1を登録し、入力された場合は2を登録する
- 担当者テーブルはあらかじめ登録されているものとする

## 照会画面 ##

- 受注テーブルよりステータスが1のものを全て照会する

## テーブル設計 ##

### 受注テーブル:t_receive ###

| 論理名 | 物理名 | 桁 | 主キー | 外部キー |
| :-- | :-- | :-- | :-- | :-- |
| No | number | 2 | ○ |  |
| 項目名 | item_name | 30 |   |
| 内訳番号 | uwk_number | 3 | ○  |   |
| 担当者番号 | staff _number | 4	 |    |○  |
| 納品日 | delivery | 8 |    |
| ステータス | status | 1  |   |

### 担当者テーブル:m_staff ###

| 論理名 | 物理名 | 桁 | 主キー |
| :-- | :-- | :-- | :-- |
| 担当者番号 | staff _number | 4 | ○ |
| 担当者名 | staff _name | 30 |  |

