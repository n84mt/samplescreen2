package sample.screen;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sample.screen.domain.Order;
import sample.screen.domain.Orders;
import sample.screen.domain.Staff;
import sample.screen.exception.AlreadyExistRecordException;
import sample.screen.exception.NotExistStaffException;
import sample.screen.exception.PastTimeDeliveryDateException;
import sample.screen.util.LocalDateConverter;

/**
 * Servlet implementation class RegisterService
 */
@WebServlet("/RegisterService")
public class RegisterService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String STR_BLANK = "";
	private static final LocalDate DELIVERY_DATE_NOT_EXIST = LocalDate.of(1900, 01, 01);
	private static final String ORDER_STATUS_NOT_EXIST = "1";
	private static final String ORDER_STATUS_EXIST = "2";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterService() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(false);

		String strTableRowNums = (String) session.getAttribute("table_row_nums");
		int intTableRowNums = Integer.parseInt(strTableRowNums);

		// フォーム必須項目未入力フラグ
		boolean formEmptyCheckFlag = false;

		// 登録済レコードありフラグ
		boolean alreadyExistRecordFlag = false;

		// メッセージセッションの破棄
		session.removeAttribute("result_message");
		session.removeAttribute("error_message");

		// 複数受注レコードDTO
		Orders orders = new Orders();

		int notEmptyFormLineCounter = 0;
		for (int i = 0; i < intTableRowNums; i++) {
			String rownumOrderNumber = "order_number_" + (i + 1);
			String rownumItemName = "item_name_" + (i + 1);
			String rownumDetailNumber = "detail_number_" + (i + 1);
			String rownumStaffNumber = "staff_number_" + (i + 1);
			String rownumDeliveryDate = "delivery_date_" + (i + 1);

			// 全項目未入力ならこの行のフォームは無視する
			if (isThisFromLineEmpty(request, rownumOrderNumber, rownumItemName, rownumDetailNumber, rownumStaffNumber,
					rownumDeliveryDate))
				continue;

			StringBuilder requiredMessageBuilder = new StringBuilder();
			int thisLineOrderNunmber = 0;
			String thisLineItemName = "";
			int thisLineDetailNumber = 0;
			int thisLineStaffNumber = 0;
			LocalDate thisLineLocalDate = DELIVERY_DATE_NOT_EXIST;
			String thisLineOrderStatus = ORDER_STATUS_NOT_EXIST;
			;

			// 入力フォーム取得： No
			if (isInputOrderNumber(request, rownumOrderNumber)) {
				thisLineOrderNunmber = Integer.parseInt(request.getParameter(rownumOrderNumber));
				session.setAttribute(rownumOrderNumber, request.getParameter(rownumOrderNumber));
			} else {
				requiredMessageBuilder.append("No");
				session.setAttribute(rownumOrderNumber, STR_BLANK);
			}

			// 入力フォーム取得： 項目名
			if (isInputItemName(request, rownumItemName)) {
				thisLineItemName = request.getParameter(rownumItemName);
				session.setAttribute(rownumItemName, request.getParameter(rownumItemName));
			} else {
				if (requiredMessageBuilder.length() != 0)
					requiredMessageBuilder.append(" ,");
				requiredMessageBuilder.append("項目名");
				session.setAttribute(rownumItemName, STR_BLANK);
			}

			// 入力フォーム取得： 内訳番号
			if (isInputDetailNumber(request, rownumDetailNumber)) {
				thisLineDetailNumber = Integer.parseInt(request.getParameter(rownumDetailNumber));
				session.setAttribute(rownumDetailNumber, request.getParameter(rownumDetailNumber));
			} else {
				if (requiredMessageBuilder.length() != 0)
					requiredMessageBuilder.append(" ,");
				requiredMessageBuilder.append("内訳番号");
				session.setAttribute(rownumDetailNumber, STR_BLANK);
			}

			// 入力フォーム取得： 担当者番号
			if (isInputStaffNumber(request, rownumStaffNumber)) {
				thisLineStaffNumber = Integer.parseInt(request.getParameter(rownumStaffNumber));
				session.setAttribute(rownumStaffNumber, request.getParameter(rownumStaffNumber));
			} else {
				if (requiredMessageBuilder.length() != 0)
					requiredMessageBuilder.append(" ,");
				requiredMessageBuilder.append("担当者番号");
				session.setAttribute(rownumStaffNumber, STR_BLANK);
			}

			// 未入力チェック結果集計
			if (requiredMessageBuilder.length() != 0) {
				requiredMessageBuilder.append(" は入力必須項目です");
				session.setAttribute("error_message", requiredMessageBuilder.toString());
				formEmptyCheckFlag = true;
				break;
			}

			// 入力フォーム取得： 納品日
			if (isInputDeliveryDate(request, rownumDeliveryDate)) {
				thisLineLocalDate = LocalDateConverter.fromStringToLocalDate(request.getParameter(rownumDeliveryDate));
				session.setAttribute(rownumDeliveryDate, request.getParameter(rownumDeliveryDate));
				thisLineOrderStatus = ORDER_STATUS_EXIST;
			} else {
				session.setAttribute(rownumDeliveryDate, STR_BLANK);
			}

			// 対象受注の担当者レコードを作成する
			Staff staff = new Staff.Builder().staffNumber(thisLineStaffNumber).build();

			// 受注レコードを作成する
			Order order = new Order.Builder().orderNumber(thisLineOrderNunmber).itemName(thisLineItemName)
					.detailNumber(thisLineDetailNumber).staff(staff).deliveryDate(thisLineLocalDate)
					.orderStatus(thisLineOrderStatus).build();

			// 登録前のリストにこの受注レコードを加える
			orders.add(order);

			// 有効な受注レコードとしてこの行をカウントする
			notEmptyFormLineCounter++;
		}

		// 全フォームに１つも入力がなければ
		if (isAllEmpty(notEmptyFormLineCounter, formEmptyCheckFlag))
			session.setAttribute("error_message", "入力がありません");

		// 有効な受注レコードが入力されていれば
		if (hasValidRecord(notEmptyFormLineCounter)) {
			try {
				orders.insertNewOrders();
				session.setAttribute("error_message", "");
			} catch (AlreadyExistRecordException e) {
				session.setAttribute("error_message", e.getMessage());
				alreadyExistRecordFlag = true;
			} catch (NotExistStaffException e) {
				session.setAttribute("error_message", e.getMessage());
				alreadyExistRecordFlag = true;
			} catch (PastTimeDeliveryDateException e) {
				session.setAttribute("error_message", e.getMessage());
				alreadyExistRecordFlag = true;
			} catch (Exception e) {
				session.setAttribute("error_message", "登録できませんでした");
				e.printStackTrace();
			}
		}

		// 有効な受注レコードであり、登録済の内容でなければ
		if (isValidAndNotDuplicated(notEmptyFormLineCounter, alreadyExistRecordFlag)) {
			if (session != null) {
				session.invalidate();
				session = request.getSession(true);
			}
			session.setAttribute("result_message", notEmptyFormLineCounter + "件の受注レコードを登録しました");
		}

		// メイン画面に遷移
		response.sendRedirect("main-screen.jsp");

	}

	// 入力有無判定： 受注番号
	private boolean isInputOrderNumber(HttpServletRequest request, String rownumOrderNumber) {
		return !"".equals(request.getParameter(rownumOrderNumber));
	}

	// 入力有無判定： 項目名
	private boolean isInputItemName(HttpServletRequest request, String rownumItemName) {
		return !"".equals(request.getParameter(rownumItemName));
	}

	// 入力有無判定：内訳番号
	private boolean isInputDetailNumber(HttpServletRequest request, String rownumDetailNumber) {
		return !"".equals(request.getParameter(rownumDetailNumber));
	}

	// 入力有無判定： 担当者番号
	private boolean isInputStaffNumber(HttpServletRequest request, String rownumStaffNumber) {
		return !"".equals(request.getParameter(rownumStaffNumber));
	}

	// 入力有無判定： 納品日
	private boolean isInputDeliveryDate(HttpServletRequest request, String rownumDeliveryDate) {
		return !"".equals(request.getParameter(rownumDeliveryDate));
	}

	// 入力有無判定： 全フォームに１つも入力がないか
	private boolean isAllEmpty(int notEmptyFormLineCounter, boolean formEmptyCheckFlag) {
		return notEmptyFormLineCounter == 0 && formEmptyCheckFlag == false;
	}

	// 入力有無判定： 受注番号
	private boolean hasValidRecord(int notEmptyFormLineCounter) {
		return notEmptyFormLineCounter != 0;
	}

	// 入力有無判定： 有効な受注レコードであり、登録済の内容でないか
	private boolean isValidAndNotDuplicated(int notEmptyFormLineCounter, boolean alreadyExistRecordFlag) {
		return notEmptyFormLineCounter != 0 && alreadyExistRecordFlag == false;
	}

	// 入力有無判定： 1行単位
	private boolean isThisFromLineEmpty(HttpServletRequest request, String rownumOrderNumber, String rownumItemName,
			String rownumDetailNumber, String rownumStaffNumber, String rownumDeliveryDate) {
		return "".equals(request.getParameter(rownumOrderNumber)) && "".equals(request.getParameter(rownumItemName))
				&& "".equals(request.getParameter(rownumDetailNumber))
				&& "".equals(request.getParameter(rownumStaffNumber))
				&& "".equals(request.getParameter(rownumDeliveryDate));
	}

}
