package sample.screen.domain;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;

import sample.screen.db.DBManager;
import sample.screen.exception.AlreadyExistRecordException;
import sample.screen.exception.PastTimeDeliveryDateException;
import sample.screen.util.LocalDateConverter;

public class Order {
	private int orderNumber;
	private String itemName;
	private int detailNumber;
	private Staff staff;
	private LocalDate deliveryDate;
	private String orderStatus;
	private static final LocalDate DELIVERY_DATE_NOT_EXIST = LocalDate.of(1900, 01, 01);
	private static final String ORDER_STATUS_NOT_EXIST = "1";

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getDetailNumber() {
		return detailNumber;
	}

	public void setDetailNumber(int detailNumber) {
		this.detailNumber = detailNumber;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public static class Builder {
		private int orderNumber = 0;
		private String itemName = "";
		private int detailNumber = 0;
		private Staff staff;
		private LocalDate deliveryDate = DELIVERY_DATE_NOT_EXIST;
		private String orderStatus = ORDER_STATUS_NOT_EXIST;

		public Builder orderNumber(int val) {
			orderNumber = val;
			return this;
		}

		public Builder itemName(String val) {
			itemName = val;
			return this;
		}

		public Builder detailNumber(int val) {
			detailNumber = val;
			return this;
		}

		public Builder staff(Staff val) {
			staff = val;
			return this;
		}

		public Builder deliveryDate(LocalDate val) {
			deliveryDate = val;
			return this;
		}

		public Builder orderStatus(String val) {
			orderStatus = val;
			return this;
		}

		public Order build() {
			return new Order(this);
		}
	}

	private Order(Builder builder) {
		orderNumber = builder.orderNumber;
		itemName = builder.itemName;
		detailNumber = builder.detailNumber;
		staff = builder.staff;
		deliveryDate = builder.deliveryDate;
		orderStatus = builder.orderStatus;
	}

	public Order() {
	}

	public DBManager insertNewOrder(DBManager db) throws Exception {
		String sql = "INSERT INTO t_order"
				+ "(order_number, item_name, detail_number, staff_number, delivery_date, order_status) "
				+ "VALUES (?, ?, ?, ?, ?, ?);";
		PreparedStatement statement = null;
		statement = db.getPreparedStatement(sql);
		statement.setInt(1, this.orderNumber);
		statement.setString(2, this.itemName);
		statement.setInt(3, this.detailNumber);
		statement.setInt(4, this.staff.getStaffNumber());
		statement.setDate(5, LocalDateConverter.fromLocalDateToSqldate(this.deliveryDate));
		statement.setString(6, this.orderStatus);
		statement.executeUpdate();
		statement.close();
		return db;
	}

	public boolean isNotExistRecord(DBManager db) throws Exception {
		String sql = "SELECT count(*) AS total FROM t_order WHERE order_number = ? AND detail_number =?;";
		PreparedStatement statement = null;
		statement = db.getPreparedStatement(sql);
		statement.setInt(1, this.orderNumber);
		statement.setInt(2, this.detailNumber);
		ResultSet rs = statement.executeQuery();
		while (rs.next()) {
			if (rs.getInt("total") == 1) {
				statement.close();
				rs.close();
				throw new AlreadyExistRecordException(
						"No=" + this.orderNumber + ", 内訳番号=" + this.detailNumber + "の受注レコードは既に登録されています");
			}
		}
		statement.close();
		rs.close();
		return true;
	}

	public boolean isNotPastTimeDeliveryDate() throws PastTimeDeliveryDateException {
		LocalDate today = LocalDate.now();
		int pastDays = today.compareTo(this.deliveryDate);
		if (hasValidDateError(pastDays)) {
			throw new PastTimeDeliveryDateException(
					"登録しようとする納品日" + LocalDateConverter.fromLocalDateToString(this.deliveryDate) + " は過去の日付です");
		}
		return true;
	}

	private boolean hasValidDateError(int pastDays) {
		return this.deliveryDate.compareTo(DELIVERY_DATE_NOT_EXIST) != 0 && 0 <= pastDays;
	}
}
