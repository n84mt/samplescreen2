package sample.screen.domain;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import sample.screen.db.DBManager;
import sample.screen.exception.PastTimeDeliveryDateException;

public class Orders {
	private List<Order> orders;

	public List<Order> getOrders() {
		return this.orders;
	}

	public void add(Order order) {
		if (this.orders == null)
			this.orders = new ArrayList<Order>();
		this.orders.add(order);
	}

	public void insertNewOrders() throws Exception {
		DBManager db = new DBManager();
		for (Order order : this.orders) {
			if (isValidRecord(db, order))
				db = order.insertNewOrder(db);
		}
		db.commit();
		db.close();
	}

	public Orders findAllwithNoDeliveryDate() throws Exception {
		Orders orders = new Orders();
		DBManager db = new DBManager();
		String sql = "select order_number, item_name, staff_name "
				+ "FROM t_order LEFT JOIN m_staff ON t_order.staff_number = m_staff.staff_number"
				+ " where order_status = '1'";
		PreparedStatement statement = db.getPreparedStatement(sql);
		ResultSet rs = statement.executeQuery();
		while (rs.next()) {
			Staff staff = new Staff();
			staff.setStaffName(rs.getString("staff_name"));
			Order order = new Order();
			order.setOrderNumber(rs.getInt("order_number"));
			order.setItemName(rs.getString("item_name"));
			order.setStaff(staff);
			orders.add(order);
		}
		statement.close();
		rs.close();
		db.close();
		return orders;
	}

	private boolean isValidRecord(DBManager db, Order order) throws PastTimeDeliveryDateException, Exception {
		return order.isNotExistRecord(db) && order.getStaff().isExistStaff(db) && order.isNotPastTimeDeliveryDate();
	}
}
