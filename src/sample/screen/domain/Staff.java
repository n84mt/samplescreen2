package sample.screen.domain;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import sample.screen.db.DBManager;
import sample.screen.exception.NotExistStaffException;

public class Staff {
	private int staffNumber;
	private String staffName;
	public int getStaffNumber() {
		return staffNumber;
	}
	public void setStaffNumber(int staffNumber) {
		this.staffNumber = staffNumber;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public static class Builder {
		private int staffNumber = 0;
		private String staffName = "";

		public Builder staffNumber(int val) {
			staffNumber = val;
			return this;
		}

		public Staff build() {
			return new Staff(this);
		}
	}

	private Staff(Builder builder) {
		staffNumber = builder.staffNumber;
		staffName = builder.staffName;
	}
	public Staff() {
	}
	public boolean isExistStaff(DBManager db) throws Exception {
		String sql = "SELECT count(*) AS total FROM m_staff WHERE staff_number = ?;";
		PreparedStatement statement = null;
		statement = db.getPreparedStatement(sql);
		statement.setInt(1, this.staffNumber);
		ResultSet rs = statement.executeQuery();
		while (rs.next()) {
			if (rs.getInt("total") == 0) {
				throw new NotExistStaffException(
						"担当者番号=" + this.staffNumber + " の担当者は存在しません（未登録）");
			}
		}
		return true;
	}

}
