package sample.screen;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AddRowService
 */
@WebServlet("/AddRowService")
public class AddRowService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddRowService() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// ③-1 モデルの情報を判定
		HttpSession session = request.getSession(false);

		String strTableRowNums = (String) session.getAttribute("table_row_nums");
		int intTableRowNums = Integer.parseInt(strTableRowNums);

		for (int i = 0; i < intTableRowNums; i++) {
			String rownumOrderNumber = "order_number_" + (i + 1);
			String rownumItemName = "item_name_" + (i + 1);
			String rownumDetailNumber = "detail_number_" + (i + 1);
			String rownumStaffNumber = "staff_number_" + (i + 1);
			String rownumDeliveryDate = "delivery_date_" + (i + 1);

			session.setAttribute(rownumOrderNumber, request.getParameter(rownumOrderNumber));
			session.setAttribute(rownumItemName, request.getParameter(rownumItemName));
			session.setAttribute(rownumDetailNumber, request.getParameter(rownumDetailNumber));
			session.setAttribute(rownumStaffNumber, request.getParameter(rownumStaffNumber));
			session.setAttribute(rownumDeliveryDate, request.getParameter(rownumDeliveryDate));
		}

		if (session.getAttribute("table_row_nums") != null) {
			strTableRowNums = (String) session.getAttribute("table_row_nums");
			strTableRowNums = String.valueOf(Integer.parseInt(strTableRowNums) + 1);
		}
		session.setAttribute("table_row_nums", strTableRowNums);
		session.setAttribute("result_message", "行を追加しました");

		// ③-1-2-1 つぎに表示させる画面（ビュー）を指定
		response.sendRedirect("main-screen.jsp");
	}
}
