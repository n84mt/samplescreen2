package sample.screen;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sample.screen.constant.ButtonType;

/**
 * Servlet implementation class ExecController
 */
@WebServlet("/ExecController")
public class ExecController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExecController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String submitType = request.getParameter("submitType");

		RequestDispatcher rd = null;

		if (isPushedReferenceButton(submitType)) {
			rd = request.getRequestDispatcher("ReferenceService");
		}

		if (isPushedRegisterButton(submitType)) {
			rd = request.getRequestDispatcher("RegisterService");
		}
		if (isPushedAddRowButton(submitType)) {
			rd = request.getRequestDispatcher("AddRowService");
		}
		rd.forward(request, response);
	}

	private boolean isPushedAddRowButton(String submitType) {
		return submitType.equals(ButtonType.ADD_ROW.buttonName());
	}

	private boolean isPushedReferenceButton(String submitType) {
		return submitType.equals(ButtonType.REFERENCE_SCREEN.buttonName());
	}

	private boolean isPushedRegisterButton(String submitType) {
		return submitType.equals(ButtonType.REGISTER.buttonName());
	}



}
