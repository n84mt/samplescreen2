package sample.screen.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBManager implements AutoCloseable {

	private Connection connection = null;

	public DBManager() {
	}

	public Connection getConnection() throws SQLException, NamingException {
		try {
			if (connection == null || connection.isClosed()) {
				InitialContext intitCtx = new InitialContext();
				DataSource dataSource = (DataSource) intitCtx.lookup("java:comp/env/jdbc/sampleScreen2");
				connection = dataSource.getConnection();
			}
		} catch (NamingException e) {
			e.printStackTrace();
			connection = null;
			throw e;
		}
		connection.setAutoCommit(false);
		return connection;
	}

	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection = null;
		}
	}

	public PreparedStatement getPreparedStatement(String sql) throws Exception {
		return getConnection().prepareStatement(sql);
	}

	public void commit() throws SQLException {
		connection.commit();
	}

	public void rollback() throws SQLException {
		connection.rollback();
	}

	@Override
    public void close() {
    	//System.out.println("close connection -------------------------------------------------->");
    	try {
    		connection.close();
    	} catch (SQLException e) {
    		e.printStackTrace();
    	} finally {
    		connection = null;
    	}
    }
}
