package sample.screen.constant;

public enum ButtonType {
    REGISTER("登録"),
    REFERENCE_SCREEN("照会画面"),
	ADD_ROW("行追加");

    private final String name;

    //コンストラクタ
    ButtonType(String name) {
        this.name = name;
    }

    public String buttonName() {
        return this.name;
    }
}
