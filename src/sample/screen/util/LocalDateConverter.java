package sample.screen.util;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateConverter {
	static final private String DATE_FORMAT = "yyyy-MM-dd";

	public static LocalDate fromStringToLocalDate(final String date) {
		if ("".equals(date))
			return LocalDate.of(1900, 01, 01);
	  return LocalDate.parse(date, DateTimeFormatter.ofPattern(DATE_FORMAT));
	}

	public static String fromLocalDateToString(final LocalDate date) {
	  return String.valueOf(date);
	}

	public static Date fromLocalDateToSqldate(final LocalDate localDate) {
		  return Date.valueOf(localDate);
		}
}
