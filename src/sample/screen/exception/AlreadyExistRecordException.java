package sample.screen.exception;

public class AlreadyExistRecordException extends Exception {
	public AlreadyExistRecordException(String string) {
		super(string);
	}
}
