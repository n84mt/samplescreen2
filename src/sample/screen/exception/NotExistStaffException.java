package sample.screen.exception;

public class NotExistStaffException extends Exception {
	public NotExistStaffException(String string) {
		super(string);
	}
}
