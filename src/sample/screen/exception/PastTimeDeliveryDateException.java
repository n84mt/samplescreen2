package sample.screen.exception;

public class PastTimeDeliveryDateException extends Exception {
	public PastTimeDeliveryDateException(String string) {
		super(string);
	}
}
