<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="orders" class="sample.screen.domain.Orders"
	scope="session" />
<%@ page import="sample.screen.domain.Order"%>
<%@ page import="sample.screen.domain.Staff"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/main.css" />
<title>照会画面</title>
</head>
<body>
	<h1>[照会画面]</h1>
	<%
		if (orders.getOrders() != null) {
			List<Order> ordersList = orders.getOrders();
	%>
	<p>
		納品日が登録された受注レコードは以下の全 <%=ordersList.size()%> 件です。
	</p>
	<table>
		<tr>
			<th>No.</th>
			<th>項目名</th>
			<th>担当者名</th>
		</tr>
		<%
			for (Order order : ordersList) {
		%>
		<tr>
			<td><%=order.getOrderNumber()%></td>
			<td><%=order.getItemName()%></td>
			<td><%=order.getStaff().getStaffName()%></td>
		</tr>
		<%
			}
		%>
	</table>
	<br />
	<%
		}
	%>

	<form action="SessionInvalidService" method="post">
		<input type="submit" value="メイン画面に行く" />
	</form>
</body>
</html>