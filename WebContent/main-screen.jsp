<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/main.css" />
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js?ver=1.8.1"
	charset="euc-jp"></script>
<title>メイン画面</title>
<%
	// 行カウント：セッション情報の null チェック
	String strTableRowNums = "";
	Object sessionTableRowNums = session.getAttribute("table_row_nums");
	if (sessionTableRowNums != null) {
		strTableRowNums = (String) sessionTableRowNums;
	}
	if (sessionTableRowNums == null) {
		strTableRowNums = "1";
		session.setAttribute("table_row_nums", strTableRowNums);
	}
	int intTableRowNums = Integer.parseInt(strTableRowNums);

	// 結果メッセージ：セッション情報の null チェック
	String strResultMessage = "";
	Object sessionResultMessage = session.getAttribute("result_message");
	if (sessionResultMessage != null) {
		strResultMessage = (String) sessionResultMessage;
	} else {
		strResultMessage = "";
	}
	// エラーメッセージ：セッション情報の null チェック
	String strErrorMessage = "";
	Object sessionErrorMessage = session.getAttribute("error_message");
	if (sessionErrorMessage != null) {
		strErrorMessage = (String) sessionErrorMessage;
	} else {
		strErrorMessage = "";
	}
%>
</head>
<body>
	<h1>[メイン画面]</h1>
	<form action="ExecController" method="post">
		<input type="submit" name="submitType" value="行追加" />
		<div class="tbl_body_container">
			<table class="tbl_body data">
				<tr>
					<th class="order_number_header">No.</th>
					<th class="item_name_header">項目名</th>
					<th class="detail_number_header">内訳番号</th>
					<th class="person_in_charge_header">担当者番号</th>
					<th class="delivery_date_header">納品日</th>
				</tr>

				<%
					for (int i = 0; i < intTableRowNums; i++) {
						String rownumOrderNumber = "order_number_" + (i + 1);
						String rownumItemName = "item_name_" + (i + 1);
						String rownumDetailNumber = "detail_number_" + (i + 1);
						String rownumStaffNumber = "staff_number_" + (i + 1);
						String rownumDeliveryDate = "delivery_date_" + (i + 1);

						String sessionThisLineOrderNumber = (String) session.getAttribute(rownumOrderNumber);
						String sessionThisLineItemName = (String) session.getAttribute(rownumItemName);
						String sessionThisLineDetailNumber = (String) session.getAttribute(rownumDetailNumber);
						String sessionThisLineStaffNumber = (String) session.getAttribute(rownumStaffNumber);
						String sessionThisLineDeliveryDate = (String) session.getAttribute(rownumDeliveryDate);

						if (sessionThisLineOrderNumber == null) sessionThisLineOrderNumber = "";
						if (sessionThisLineItemName == null) sessionThisLineItemName = "";
						if (sessionThisLineDetailNumber == null) sessionThisLineDetailNumber = "";
						if (sessionThisLineStaffNumber == null) sessionThisLineStaffNumber = "";
						if (sessionThisLineDeliveryDate == null) sessionThisLineDeliveryDate = "";
				%>
				<tr>
					<td class="order_number_column"><input
						class="order_number_text-form" pattern="[0-9]{1,4}"
						title="数字で4文字以内" type="text" name="<%=rownumOrderNumber%>"
						value="<%=sessionThisLineOrderNumber%>" /></td>

					<td class="item_name_column"><input
						class="item_name_text-form" pattern=".{0,25}" title="25文字以内"
						type="text" name="<%=rownumItemName%>"
						value="<%=sessionThisLineItemName%>" /></td>

					<td class="detail_number_column"><input
						class="detail_number_text-form" pattern="[0-9]{1,4}"
						title="数字で4文字以内" type="text" name="<%=rownumDetailNumber%>"
						value="<%=sessionThisLineDetailNumber%>" /></td>

					<td class="staff_number_column"><input
						class="staff_number_text-form" pattern="[0-9]{1,5}"
						title="数字で5文字以内" type="text" name="<%=rownumStaffNumber%>"
						value="<%=sessionThisLineStaffNumber%>" /></td>

					<td class="delivery_date_column"><input
						class="delivery_date_text-form"
						pattern="([1-2]{1}[0-9]{3})\-([0-1][0-9])\-([0-3][0-9])"
						title="英数字で yyyy-MM-dd" type="text" name="<%=rownumDeliveryDate%>"
						value="<%=sessionThisLineDeliveryDate%>" /></td>
				</tr>
				<%
					}
				%>
			</table>
		</div>
		<input type="submit" name="submitType" value="照会画面" /> <input
			type="submit" name="submitType" value="登録" />
	</form>
	<p class="result-message"><%=strResultMessage%></p>
	<p class="error-message"><%=strErrorMessage%></p>
</body>
</html>